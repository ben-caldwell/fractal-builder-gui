from kivy.config import Config
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')
Config.set('graphics', 'width', '640')
Config.set('graphics', 'height', '600')
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.behaviors import DragBehavior, CompoundSelectionBehavior
from kivy.properties import NumericProperty, ObjectProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.graphics import Line, Point, InstructionGroup, Color
from kivy.resources import resource_add_path
import os
import sys
import math
import random

SELECTED_COLOUR = Color(0.5, 0.75, 1.0)
WHITE = Color(1, 1, 1)
BLACK = Color(0, 0, 0)


class Vector2D():

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def dot(self, v):
        return self.x*v.x + self.y*v.y

    def cross(self, v):
        return self.x*v.y - self.y*v.x

    def mag(self):
        return math.sqrt(self.dot(self))

    def sub(self, v):
        return Vector2D(self.x - v.x, self.y - v.y)
    
    def add(self, v):
        return Vector2D(self.x + v.x, self.y + v.y)


class TransformationTriangle():

    v1 = Vector2D(0.0, 0.0)
    v2 = Vector2D(0.5, 1.0)
    v3 = Vector2D(1.0, 0.0)

    def transform(self, t):
        self.v1 = t.apply(self.v1.x, self.v1.y)
        self.v2 = t.apply(self.v2.x, self.v2.y)
        self.v3 = t.apply(self.v3.x, self.v3.y)

    def get_scaled_vertices(self, width, height):
        scaled_v1 = Vector2D(self.v1.x*width, self.v1.y*height)
        scaled_v2 = Vector2D(self.v2.x*width, self.v2.y*height)
        scaled_v3 = Vector2D(self.v3.x*width, self.v3.y*height)
        return scaled_v1, scaled_v2, scaled_v3
        
    def get_points(self, width, height):
        scaled_v1, scaled_v2, scaled_v3 = self.get_scaled_vertices(width, height)

        return [
                scaled_v1.x, scaled_v1.y, 
                scaled_v2.x, scaled_v2.y, 
                scaled_v3.x, scaled_v3.y,
                scaled_v1.x, scaled_v1.y
            ]

    def sign(self, p1, p2, p3):
        return (p1.x- p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y)

    def on(self, x, y, width, height):
        scaled_v1, scaled_v2, scaled_v3 = self.get_scaled_vertices(width, height)

        v = Vector2D(x, y)
        d1 = self.sign(v, scaled_v1, scaled_v2)
        d2 = self.sign(v, scaled_v2, scaled_v3)
        d3 = self.sign(v, scaled_v3, scaled_v1)

        has_neg = (d1 < 0) or (d2 < 0) or (d3 < 0)
        has_pos = (d1 > 0) or (d2 > 0) or (d3 > 0)

        is_on = not (has_neg and has_pos)

        return is_on


class Transformation(Widget):
    rotation = NumericProperty(0.0)
    x_translation = NumericProperty(0.0)
    y_translation = NumericProperty(0.0)
    x_scale = NumericProperty(1.0)
    y_scale = NumericProperty(1.0)
    probability = NumericProperty(1.0)
    
    triangle = TransformationTriangle()

    selected = False
    
    def __init__(self, parent, **kwargs):
        super().__init__(**kwargs)
        self.bind(x_translation=self.update)
        self.bind(y_translation=self.update)
        self.bind(x_scale=self.update)
        self.bind(y_scale=self.update)
        self.bind(rotation=self.update)
        self.parent = parent
    
    def render(self):
        self.triangle = TransformationTriangle()
        self.triangle.transform(self)
        points = self.triangle.get_points(self.parent.width, self.parent.height)
        display_triangle = Line(points=points)
        return display_triangle

    def apply(self, x, y):
        angle = math.radians(self.rotation)
        x, y = x*self.x_scale,                          y*self.y_scale
        x, y = x*math.cos(angle) - y*math.sin(angle),   x*math.sin(angle) + y*math.cos(angle)
        x, y = x + self.x_translation,                  y + self.y_translation

        return Vector2D(x, y)

    def update(self, *kwargs):
        self.parent.update()

    def select(self):
        self.selected = True
        self.update()

    def deselect(self):
        self.selected = False
        self.update()


class Fractal():

    transformations = []

    def __init__(self, transformations=[], origin=(0.0, 0.0)):
        self.transformations = transformations
        self.origin = origin

    def apply(self, x, y):
        if len(self.transformations) == 0:
            return 0., 0.

        t = self.get_random_transform()
        return t.apply(x, y)
        
    def get_random_transform(self):
        return self.transformations[random.randint(0, len(self.transformations)-1)]
        r = random.random()
        for t in self.transformations:
            if r < t.probability:
                return t
            r -= t.probability

    def add_transformation(self, t):
        self.transformations.append(t)
        self.update_probabilities()
    
    def remove_transformation(self, t):
        self.transformations.remove(t)
        self.update_probabilities()
    
    def update_probabilities(self):
        sum_of_scales = 0
        for t in self.transformations:
            sum_of_scales += t.x_scale * t.y_scale

        for t in self.transformations:
            t.probability = (t.x_scale * t.y_scale)/sum_of_scales

    def remove_selected(self):
        for t in self.transformations:
            if t.selected:
                self.remove_transformation(t)

   



class FractalWidget(Widget):
    fractal = Fractal()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


    """ 
    ==========================================
    Mouse controls for editing transformations
    ==========================================
    """    

    selected = None

    down_rotation = 0
    down_x_translation = 0
    down_y_translation = 0
    down_x = 0
    down_y = 0

    def on_touch_down(self, touch):
        if touch.button == 'left' or touch.button == 'right':
            found_triangle = False
            for t in self.fractal.transformations:
                if t.triangle.on(touch.x, touch.y, self.width, self.height):
                    self.deselect()
                    self.select(t)
                    self.down_x = touch.x
                    self.down_y = touch.y
                    self.down_x_translation = t.x_translation
                    self.down_y_translation = t.y_translation
                    self.down_rotation = t.rotation
                    self.down_vector = self.get_vector_to_origin(touch)
                    found_triangle = True

            if not found_triangle:
                self.deselect()

        elif touch.is_mouse_scrolling:
            if self.selected is not None:
                if touch.button == 'scrollup':
                    self.selected.x_scale *= 0.95
                    self.selected.y_scale *= 0.95
                elif touch.button == 'scrolldown':
                    self.selected.x_scale *= 1.05
                    self.selected.y_scale *= 1.05

                
        
    def on_touch_move(self, touch):
        if self.selected is None:
            return
        
        delta_x = (touch.x - self.down_x)/self.width
        delta_y = (touch.y - self.down_y)/self.height

        if touch.button == 'left':
            self.selected.x_translation = self.down_x_translation + delta_x
            self.selected.y_translation = self.down_y_translation + delta_y
        
        elif touch.button == 'right':
            current_vector = self.get_vector_to_origin(touch)
            angle = self.calculate_angle_between(current_vector, self.down_vector)
            self.selected.rotation = int(self.down_rotation + angle)
            
    def get_vector_to_origin(self, touch):
        v1 = self.selected.triangle.get_scaled_vertices(self.width, self.height)[0]
        return Vector2D(touch.x, touch.y).sub(v1)

    def calculate_angle_between(self, v1, v2):
        costheta = v1.dot(v2)/(v1.mag()*v2.mag())
        if costheta > 1:
            costheta = 1
        elif costheta < -1:
            costheta = -1

        angle = math.degrees(math.acos(costheta))
        if v1.cross(v2) > 0:
            angle *= -1
        
        return angle

    def select(self, t):
        t.select()
        self.selected = t

    def deselect(self):
        if self.selected is None:
            return
        self.selected.deselect()
        self.selected = None


    """ 
    ==========================================
    Rendering the fractal
    ==========================================
    """ 

    points_group = InstructionGroup()
    show_triangles = True

    def render_fractal(self, iterations, pointsize):
        self.points_group.clear()

        max_num_points_in_each_group = 2**14
        points_i = None
        
        x = y = 0
        for i in range(iterations):
            j = i % max_num_points_in_each_group
            if j == 0:
                points_i = Point(points=[], pointsize=pointsize)
                self.points_group.add(points_i)
                

            new_point = self.fractal.apply(x, y)
            x = new_point.x
            y = new_point.y
            display_x = (self.fractal.origin[0] + x) * self.width
            display_y = (self.fractal.origin[1] + y) * self.height

            points_i.add_point(display_x, display_y)

        self.canvas.add(self.points_group)

    def render(self, iterations, pointsize):
        self.canvas.clear()

        if len(self.fractal.transformations) == 0:
            return

        self.canvas.add(WHITE)
        self.render_fractal(iterations, pointsize)

        if self.show_triangles:
            for t in self.fractal.transformations:
                if t.selected:
                    self.canvas.add(SELECTED_COLOUR)
                else:
                    self.canvas.add(WHITE)
                    
                self.canvas.add(t.render())
        

    """ 
    ==========================================
    Buttons and sliders for editing parameters
    ==========================================
    """ 

    def add_transformation(self):
        t = Transformation(self)
        self.fractal.add_transformation(t)
        self.update()

    def remove_selected(self):
        self.fractal.remove_selected()
        self.update()

    def update(self):
        self.parent.update()

    def toggle_triangles(self):
        self.show_triangles = not self.show_triangles
        self.update()


class FractalEditorWidget(BoxLayout):
    fractal_widget = ObjectProperty(None)
    short_iterations = NumericProperty(1000)
    long_iterations = NumericProperty(200000)
    short_pointsize = NumericProperty(0.8)
    long_pointsize = NumericProperty(0.12)
    
    def add_transformation(self):
        self.fractal_widget.add_transformation()

    def remove_selected(self):
        self.fractal_widget.remove_selected()

    def partial_render(self):
        self.fractal_widget.render(self.short_iterations, self.short_pointsize)

    def full_render(self):
        self.fractal_widget.render(self.long_iterations, self.long_pointsize)

    def update(self):
        self.partial_render()


class FractalApp(App):
    def build(self):
        return FractalEditorWidget()


def resourcePath():
    '''Returns path containing content - either locally or in pyinstaller tmp file'''
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS)

    return os.path.join(os.path.abspath("."))

if __name__ == '__main__':
    resource_add_path(resourcePath())
    FractalApp().run()