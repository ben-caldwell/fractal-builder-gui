# Fractal Explorer

A GUI for building Iterated Functions Systems for generating Fractals, developed in Python using Kivy.

## Installation
Just download the FractalExplorer.exe file located [here](https://gitlab.com/ben-caldwell/fractal-builder-gui/-/blob/master/application/fractalexplorer.exe), it should work with no other files.

## Controls
* Sidebar:
    * \+ button adds a new transformation
    * \- deletes the selected transformation
    * Δ shows/hides the triangles that represent the transformations
    * R renders the image with a much larger number of iterations using smaller points so the fractal is in much higher detail, although takes longer than the "low definition" rendering done while editing the transformations
* Click inside a triangle to select that transformation
* Click and drag to move transformations around
* Right click and drag to rotate transformation around the origin (bottom left)
* Scroll up/down to enlarge/shrink the transformation

## Algorithm

The random algorithm is used to render the fractals from the set of transformations.

All fractals are rendered with origin (0, 0) being the bottom left of the screen.

## Features not yet added
* Adjust horizontal scale and vertical scale separately
* Be able to flip transformations and add a way to identify if a triangle has been flipped
* Adjust the number of iterations done to render
* Manually input values for the transformation parameters
* Output the coefficients for the IFS
* Button to export the image
* Improve UI, add icons to the buttons